package syafrie.faisal.uts1april

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_log.view.*

class FragmentLog : Fragment(), View.OnClickListener {

    lateinit var thisParent : MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    var idlog : String = ""


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDbObject()

        v = inflater.inflate(R.layout.frag_data_log, container, false)
        v.btnDelete.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsLog.setOnItemClickListener(itemClick)
        return v
    }

    fun showDataLog(){
        val cursor : Cursor = db.query("log", arrayOf("id_log as _id","tanggal","nama", "hasil"),
            null,null,null,null,"id_log asc")
        adapter = SimpleCursorAdapter(thisParent, R.layout.item_data_log,cursor,
            arrayOf("_id","tanggal","nama","hasil"), intArrayOf(R.id.txId, R.id.txTanggal, R.id.txNama, R.id.txHasil),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsLog.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataLog()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDelete ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()

            }
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idlog = c.getString(c.getColumnIndex("_id"))
    }

    fun deleteDataLog(idlog: String){
        db.delete("log","id_log = $idlog",null)
        showDataLog()
    }


    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataLog(idlog)
    }
}
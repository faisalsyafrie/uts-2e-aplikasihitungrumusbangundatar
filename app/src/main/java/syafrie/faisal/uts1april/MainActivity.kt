package syafrie.faisal.uts1april

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView


import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    lateinit var db : SQLiteDatabase
    lateinit var fragLog : FragmentLog
    lateinit var fragpersegi : FragmentPersegi
    lateinit var fragjajar : FragmentJajar
    lateinit var fragketupat : FragmentKetupat
    lateinit var fraglayang : FragmentLayang
    lateinit var fraglingkaran : FragmentLingkaran
    lateinit var fragpersegip : FragmentPersegiPanjang
    lateinit var fragsegitiga : FragmentSegitiga
    lateinit var fragtrapesium : FragmentTrapesium
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragLog = FragmentLog()
        fragpersegi = FragmentPersegi()
        fragjajar = FragmentJajar()
        fraglayang = FragmentLayang()
        fragketupat = FragmentKetupat()
        fraglingkaran = FragmentLingkaran()
        fragpersegip = FragmentPersegiPanjang()
        fragsegitiga = FragmentSegitiga()
        fragtrapesium = FragmentTrapesium()
        db = DBOpenHelper(this).writableDatabase
        button.setOnClickListener(this)
    }

    fun getDbObject() : SQLiteDatabase {
        return db
    }
	
	override fun onClick(v:View?){
		var popMenu = PopupMenu(this,v)
		popMenu.menuInflater.inflate(R.menu.menu_popup,popMenu.menu)
		popMenu.setOnMenuItemClickListener{ item->
			when(item.itemId){
			R.id.itemPersegi->{

                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragpersegi).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                frameLayout.visibility = View.VISIBLE
                true
			}
                R.id.itemJajar->{

                    ft = supportFragmentManager.beginTransaction()
                    ft.replace(R.id.frameLayout,fragjajar).commit()
                    frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                    frameLayout.visibility = View.VISIBLE
                    true
                }
                R.id.itemPersegiPanjang->{

                    ft = supportFragmentManager.beginTransaction()
                    ft.replace(R.id.frameLayout,fragpersegip).commit()
                    frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                    frameLayout.visibility = View.VISIBLE
                    true
                }
                R.id.itemKetupat->{

                    ft = supportFragmentManager.beginTransaction()
                    ft.replace(R.id.frameLayout,fragketupat).commit()
                    frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                    frameLayout.visibility = View.VISIBLE
                    true
                }
                R.id.itemLayang->{

                    ft = supportFragmentManager.beginTransaction()
                    ft.replace(R.id.frameLayout,fraglayang).commit()
                    frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                    frameLayout.visibility = View.VISIBLE
                    true
                }
                R.id.itemLingkaran->{

                    ft = supportFragmentManager.beginTransaction()
                    ft.replace(R.id.frameLayout,fragketupat).commit()
                    frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                    frameLayout.visibility = View.VISIBLE
                    true
                }
                R.id.itemSegitiga->{

                    ft = supportFragmentManager.beginTransaction()
                    ft.replace(R.id.frameLayout,fragsegitiga).commit()
                    frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                    frameLayout.visibility = View.VISIBLE
                    true
                }
                R.id.itemTrapesium->{

                    ft = supportFragmentManager.beginTransaction()
                    ft.replace(R.id.frameLayout,fragtrapesium).commit()
                    frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                    frameLayout.visibility = View.VISIBLE
                    true
                }
			
			}
		    false
		}
        popMenu.show()
	}

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemLog ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragLog).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                frameLayout.visibility = View.VISIBLE

            }

            R.id.itemHitung ->frameLayout.visibility = View.GONE

        }
        return true
    }
}

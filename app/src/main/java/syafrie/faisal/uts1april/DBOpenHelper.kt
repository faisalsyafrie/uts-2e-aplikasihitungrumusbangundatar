package syafrie.faisal.uts1april

import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.content.Context

class DBOpenHelper(context : Context) : SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tLog = "create table log(id_log integer primary key autoincrement, tanggal date, nama text not null, hasil text not null)"

        db?.execSQL(tLog)


    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "bangun1"
        val DB_Ver = 1
    }

}
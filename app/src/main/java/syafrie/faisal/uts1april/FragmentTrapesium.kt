package syafrie.faisal.uts1april

import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_trapesium.*
import kotlinx.android.synthetic.main.frag_data_trapesium.view.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class FragmentTrapesium: Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
	lateinit var thisParent : MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    var idProdi : String = ""



    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    var s1 : Double = 0.0
    var s2 : Double = 0.0
    var s3 : Double = 0.0
    var s4 : Double = 0.0
    var t : Double = 0.0
    var hasil : Double = 0.0
    var hasil2 : Double = 0.0
	var hasilSemua : String =""
	
    override fun onClick(v: View?) {
        s1 = sisi.text.toString().toDouble()
        s2 = editText2.text.toString().toDouble()
        s3 = editText.text.toString().toDouble()
        s4 = editText3.text.toString().toDouble()
        t = editText5.text.toString().toDouble()
        hasil2 = s1+s2+s3+s4
        hasil = (s1+s2)*t/2
        luas.text = DecimalFormat("#.##").format(hasil)
        keliling.text = DecimalFormat("#.##").format(hasil2)
		hasilSemua = "S1: "+sisi.text+" | S2: "+editText2.text+" | Tinggi: "+editText5.text+" | L: "+luas.text+" | K: "+keliling.text
		
		
		when(v?.id){
            R.id.btnInsert ->{
                builder.setTitle("Konfirmasi").setMessage("Simpan hasil?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            
        }
    }
	
	 fun insertDataLog(hasilSemua : String){
		var dateFormat : SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
		var date = Date()
		
        var cv : ContentValues = ContentValues()
       
        cv.put("tanggal", dateFormat.format(date))
		cv.put("nama","Trapesium")
		cv.put("hasil",hasilSemua)
		db.insert("log",null,cv)
        
    }
	
	   val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataLog(hasilSemua)
        v.sisi.setText("")
        v.editText.setText("")
        v.editText2.setText("")
        v.editText3.setText("")
        v.editText5.setText("")
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_trapesium,container,false)
        db = thisParent.getDbObject()
        builder = AlertDialog.Builder(thisParent)
        v.btnHasil.setOnClickListener(this)
		v.btnInsert.setOnClickListener(this)


        return v
    }



    override fun onStart() {
        super.onStart()

    }
}